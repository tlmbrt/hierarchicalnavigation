﻿using Xamarin.Forms;

namespace HierarchicalNavigation
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void OnNextPageTapped(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new SecondPage(usernameEntry.Text));
        }
    }
}
