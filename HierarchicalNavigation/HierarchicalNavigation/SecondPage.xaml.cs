﻿using Xamarin.Forms;

namespace HierarchicalNavigation
{
	public partial class SecondPage : ContentPage
	{
		public SecondPage(string username)
		{
			InitializeComponent();

            usernameLabel.Text = string.IsNullOrEmpty(username) ?
                "Come back with an username" :
                "Welcome " + username;
		}
	}
}